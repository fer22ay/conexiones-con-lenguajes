#!/usr/bin/perl
use strict;
use DBI;


print <<EOF;
Content-type: text/html

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Bootstrap Example</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

EOF
print "";

my $db = DBI->connect("DBI:mysql:database=UNIVERSIDAD;host=localhost","fernando","MANAGER1");
my $query = $db->prepare("SELECT * FROM ESTUDIANTE");
$query->execute();

print "<body><form><center>";
print "<table class='table table-hover'>";
print "<h1>DATOS DE LA TABLA ESTUDIANTE</h1>";
print "<thead class='thead-dark'>";
	print "<th>ID ESTUDIANTE</th><th>PRIMER NOMBRE</th><th>SEGUNDO NOMBRE</th><th>ID CARRERA</th>\n";
while(my $ref = $query->fetchrow_hashref()){
print "</thead>";
	print "<tr><td align='center'>$ref->{ID_ESTUDIANTE}\t</td><td> $ref->{PRIMER_NOMBRE}</td>
	<td>$ref->{SEGUNDO_NOMBRE}</td><td align='center'>$ref->{ID_CARRERA}</td></tr>\n";
}
print "</table>";
print "</center></form></body></html>";


$query->finish();
$db->disconnect();
