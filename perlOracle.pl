#!/usr/bin/perl
use strict;
use DBI;

my $db = DBI->connect('dbi:Oracle:host=localhost;sid=XE;port=1539','FERNANDO','MANAGER1');
my $query = $db->prepare("SELECT * FROM ESTUDIANTES");
$query->execute();

print "Content-type: text/html\n\n";
print "<html><body><form><center><table border='1'>";
print "<h1>DATOS DE LA TABLA ESTUDIANTE ORACLE</h1>";

while(my $ref = $query->fetchrow_hashref()){
	print "<tr><td><h2>$ref->{ID_ESTUDIANTE}\t</h2></td><td><h2>$ref->{PRIMER_NOMBRE}</h2></td></tr>\n";
}
print "</table></center></form></body></html>\n";

$query->finish();
$db->disconnect();
